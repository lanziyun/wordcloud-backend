# wordcloud

> A Node.js project

## Build Setup

``` bash
#clone project
git clone https://lanziyun@bitbucket.org/lanziyun/wordcloud-backend.git

# install dependencies
npm install

# serve with hot reload at localhost:8090
npm run start

#api.js為執行檔


# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
