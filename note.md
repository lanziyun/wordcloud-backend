### 在 debian (buster-slim) 安裝 wordcloud 專案（未完成）

# install packages
```
sudo apt-get update
sudo apt-get install -y curl git
```

# Using Debian, as root
```
sudo curl -sL https://deb.nodesource.com/setup_lts.x | sudo bash -
sudo apt-get install -y nodejs
```

# download code
```
mkdir web && cd web
git clone https://bitbucket.org/lanziyun/wordcloud-backend.git
git clone https://bitbucket.org/lanziyun/wordcloud-vue.git
```

# run code
```
cd wordcloud-backend
npm install
npm run start&

cd wordcloud-vue
npm install
npm run start&
```