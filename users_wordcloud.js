class wordcloud {
    constructor(id, number, email, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, Q11, Q12, Q13, Q14, Q15, Q16, Q17, time) {
        this.id = id;
        this.number = number;
        this.email = email;
        this.Q1 = Q1;
        this.Q2 = Q2;
        this.Q3 = Q3;
        this.Q4 = Q4;
        this.Q5 = Q5;
        this.Q6 = Q6;
        this.Q7 = Q7;
        this.Q8 = Q8;
        this.Q9 = Q9;
        this.Q10 = Q10;
        this.Q11 = Q11;
        this.Q12 = Q12;
        this.Q13 = Q13;
        this.Q14 = Q14;
        this.Q15 = Q15;
        this.Q16 = Q16;
        this.Q17 = Q17;
        this.time = time;
    }
}