class levelup {
    constructor(id, number, email, related, point1, point2, point3, point4, point5, point6) {
        this.id = id;
        this.number = number;
        this.email = email;
        this.related = related;
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
        this.point4 = point4;
        this.point5 = point5;
        this.point6 = point6;
    }
}