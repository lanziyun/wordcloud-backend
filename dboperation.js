var config = require('./dbconfig');
const sql = require('mssql');
const { response, request } = require('express');
const { user } = require('./dbconfig');

async function getUser() { //async才可以用await
    try {
        let pool = await sql.connect(config); //await是強制執行一行一行執行下去
        let users = await pool.request().query("SELECT Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8,Q9,Q10,Q11,Q12,Q13,Q14,Q15,Q16,Q17 FROM wordcloud");
        console.log('get all success');
        return users.recordsets;
    }
    catch (error) {
        console.log('connect error', error);
    }
}

async function getUserId(userId) {
    try {
        let pool = await sql.connect(config);
        let user = await pool.request()
            .input('userId', sql.Int, userId)//''裡的就是postman要打的與下方@一致
            .query("SELECT * FROM wordcloud where id = @userId");
        console.log('get one success');
        return user.recordsets;
    }
    catch (error) {
        console.log('connect error', error);
    }
}

async function createUsersType(user) {
    try {
        let pool = await sql.connect(config);
        let insertUsertype = await pool.request()
            .input('number', sql.Int, user.number)
            .input('email', sql.NVarChar, user.email)
            .input('related', sql.NChar(10), user.related)
            .input('point1', sql.NChar(10), user.point1)
            .input('point2', sql.NChar(10), user.point2)
            .input('point3', sql.NChar(10), user.point3)
            .input('point4', sql.NChar(10), user.point4)
            .input('point5', sql.NChar(10), user.point5)
            .input('point6', sql.NChar(10), user.point6)
            .query("insert into point (number, email, related, point1, point2, point3, point4, point5, point6) values (@number, @email, @related , @point1, @point2, @point3, @point4, @point5, @point6)");
        console.log('insert type success');
        return insertUsertype.recordsets;
    }
    catch (error) {
        console.log('connect error', error);
    }
}
/*-------------------------------------------------------------------------------*/
async function updateUsersPoint1(user) {
    try {
        let pool = await sql.connect(config);
        let insertUsertype = await pool.request()
            .input('number', sql.Int, user.number)
            .input('email', sql.NVarChar, user.email)
            .input('point1', sql.NChar(10), user.point1)
            .query("UPDATE point SET point1 = @point1 WHERE number=@number AND email=@email;");
        console.log('update point1 success');
        return insertUsertype.recordsets;
    }
    catch (error) {
        console.log('connect error', error);
    }
}
async function updateUsersPoint2(user) {
    try {
        let pool = await sql.connect(config);
        let insertUsertype = await pool.request()
            .input('number', sql.Int, user.number)
            .input('email', sql.NVarChar, user.email)
            .input('point2', sql.NChar(10), user.point2)
            .query("UPDATE point SET point2 = @point2 WHERE number=@number AND email=@email;");
        console.log('update point2 success');
        return insertUsertype.recordsets;
    }
    catch (error) {
        console.log('connect error', error);
    }
}
async function updateUsersPoint3(user) {
    try {
        let pool = await sql.connect(config);
        let insertUsertype = await pool.request()
            .input('number', sql.Int, user.number)
            .input('email', sql.NVarChar, user.email)
            .input('point3', sql.NChar(10), user.point3)
            .query("UPDATE point SET point3 = @point3 WHERE number=@number AND email=@email;");
        console.log('update point3 success');
        return insertUsertype.recordsets;
    }
    catch (error) {
        console.log('connect error', error);
    }
}
async function updateUsersPoint4(user) {
    try {
        let pool = await sql.connect(config);
        let insertUsertype = await pool.request()
            .input('number', sql.Int, user.number)
            .input('email', sql.NVarChar, user.email)
            .input('point4', sql.NChar(10), user.point4)
            .query("UPDATE point SET point4 = @point4 WHERE number=@number AND email=@email;");
        console.log('update point4 success');
        return insertUsertype.recordsets;
    }
    catch (error) {
        console.log('connect error', error);
    }
}
async function updateUsersPoint5(user) {
    try {
        let pool = await sql.connect(config);
        let insertUsertype = await pool.request()
            .input('number', sql.Int, user.number)
            .input('email', sql.NVarChar, user.email)
            .input('point5', sql.NChar(10), user.point5)
            .query("UPDATE point SET point5 = @point5 WHERE number=@number AND email=@email;");
        console.log('update point5 success');
        return insertUsertype.recordsets;
    }
    catch (error) {
        console.log('connect error', error);
    }
}
async function updateUsersPoint6(user) {
    try {
        let pool = await sql.connect(config);
        let insertUsertype = await pool.request()
            .input('number', sql.Int, user.number)
            .input('email', sql.NVarChar, user.email)
            .input('point6', sql.NChar(10), user.point6)
            .query("UPDATE point SET point6 = @point6 WHERE number=@number AND email=@email;");
        console.log('update point6 success');
        return insertUsertype.recordsets;
    }
    catch (error) {
        console.log('connect error', error);
    }
}

/*-------------------------------------------------------------------------------*/
async function checkUsersType(userEmail) {
    return new Promise(async (resolve, reject) => {
        try {
            var judge = false;
            let pool = await sql.connect(config)
            let healthUser = pool.request()
                .input('number', sql.Int, userEmail.number)
                .input('email', sql.NVarChar, userEmail.email)
                .query("select * from point where email=@email and number=@number", (err, result) => {
                    if (err) {
                        console.log(err);
                    }
                    var myarr = new Array();
                    if (result.rowsAffected == 1) {
                        judge = true;
                    }
                    for (var i = 0; i < result.recordset.length; ++i) {
                        var id = result.recordset[i].id;
                        var number = result.recordset[i].number;
                        var email = result.recordset[i].email;
                        var related = result.recordset[i].related;
                        var point1 = result.recordset[i].point1;
                        var point2 = result.recordset[i].point2;
                        var point3 = result.recordset[i].point3;
                        var point4 = result.recordset[i].point4;
                        var point5 = result.recordset[i].point5;
                        var point6 = result.recordset[i].point6;
                        myarr.push({ 'id': id, 'number': number, 'email': email, 'related': related, 'point1': point1, 'point2': point2, 'point3': point3, 'point4': point4, 'point5': point5, 'point6': point6, 'judge': judge });

                    }
                    console.log(myarr)
                    resolve(myarr)
                    return myarr;
                });
        } catch (error) {
            reject(error)
            console.log('connect error', error)
        }
    })
}

async function addHealthUsers(user) {
    try {
        let pool = await sql.connect(config);
        let insertUser = await pool.request()
            .input('number', sql.Int, user.number)
            .input('email', sql.NVarChar, user.email)
            .input('Q1', sql.NChar(10), user.Q1)
            .input('Q2', sql.NChar(10), user.Q2)
            .input('Q3', sql.NChar(10), user.Q3)
            .input('Q4', sql.NChar(10), user.Q4)
            .input('Q5', sql.NChar(10), user.Q5)
            .input('name', sql.NChar(20), user.name)
            .input('phone', sql.NChar, user.phone)
            .input('contact_name', sql.NChar(20), user.contact_name)
            .input('contact_phone', sql.NChar, user.contact_phone)
            .input('time', sql.NVarChar, user.time)
            .query("insert into health (number, email, Q1, Q2, Q3, Q4, Q5, name, phone, contact_name, contact_phone, time) values (@number, @email, @Q1, @Q2, @Q3, @Q4, @Q5, @name, @phone, @contact_name, @contact_phone, @time)");
        console.log('insert health success:');
        return insertUser.recordsets;
    }
    catch (error) {
        console.log('connect error', error);
    }
}

async function checkHealthUsers(userEmail) {
    return new Promise(async (resolve, reject) => {
        try {
            var judge = false;
            let pool = await sql.connect(config)
            let healthUser = pool.request()
                .input('number', sql.Int, userEmail.number)
                .input('email', sql.NVarChar, userEmail.email)
                .query("select * from health where email=@email and number=@number", (err, result) => {
                    if (err) {
                        console.log(err);
                    }
                    if (result.rowsAffected >= 1) {
                        judge = true;
                    }
                    resolve(judge)
                    console.log(judge)
                    return judge;
                });
        } catch (error) {
            reject(error)
            console.log('connect error', error)
        }
    })

}

async function addUsers(user) {
    try {
        let pool = await sql.connect(config);
        let insertUser = await pool.request()
            .input('number', sql.Int, user.number)
            .input('email', sql.NVarChar, user.email)
            .input('Q1', sql.NChar(10), user.Q1)
            .input('Q2', sql.NChar(10), user.Q2)
            .input('Q3', sql.NChar(10), user.Q3)
            .input('Q4', sql.NChar(10), user.Q4)
            .input('Q5', sql.NChar(10), user.Q5)
            .input('Q6', sql.NChar(10), user.Q6)
            .input('Q7', sql.NChar(10), user.Q7)
            .input('Q8', sql.NChar(10), user.Q8)
            .input('Q9', sql.NChar(10), user.Q9)
            .input('Q10', sql.NChar(10), user.Q10)
            .input('Q11', sql.NChar(10), user.Q11)
            .input('Q12', sql.NChar(10), user.Q12)
            .input('Q13', sql.NChar(10), user.Q13)
            .input('Q14', sql.NChar(10), user.Q14)
            .input('Q15', sql.NChar(10), user.Q15)
            .input('Q16', sql.NChar(10), user.Q16)
            .input('Q17', sql.NChar(10), user.Q17)
            .input('time', sql.NVarChar, user.time)
            .query("insert into wordcloud (number, email, Q1, Q2, Q3, Q4, Q5,Q6,Q7,Q8,Q9,Q10,Q11,Q12,Q13,Q14,Q15,Q16,Q17, time) values (@number, @email, @Q1, @Q2, @Q3, @Q4, @Q5,@Q6,@Q7,@Q8,@Q9,@Q10,@Q11,@Q12,@Q13,@Q14,@Q15,@Q16,@Q17, @time)");
        console.log('insert success');
        return insertUser.recordsets;
    }
    catch (error) {
        console.log('connect error', error);
    }
}

async function updateUsers(user) {
    try {
        let pool = await sql.connect(config);
        let updateUser = await pool.request()
            .input('number', sql.Int, user.number)
            .input('email', sql.NVarChar, user.email)
            .input('Q1', sql.NChar(10), user.Q1)
            .input('Q2', sql.NChar(10), user.Q2)
            .input('Q3', sql.NChar(10), user.Q3)
            .input('Q4', sql.NChar(10), user.Q4)
            .input('Q5', sql.NChar(10), user.Q5)
            .input('Q6', sql.NChar(10), user.Q6)
            .input('Q7', sql.NChar(10), user.Q7)
            .input('Q8', sql.NChar(10), user.Q8)
            .input('Q9', sql.NChar(10), user.Q9)
            .input('Q10', sql.NChar(10), user.Q10)
            .input('Q11', sql.NChar(10), user.Q11)
            .input('Q12', sql.NChar(10), user.Q12)
            .input('Q13', sql.NChar(10), user.Q13)
            .input('Q14', sql.NChar(10), user.Q14)
            .input('Q15', sql.NChar(10), user.Q15)
            .input('Q16', sql.NChar(10), user.Q16)
            .input('Q17', sql.NChar(10), user.Q17)
            .input('time', sql.NVarChar, user.time)
            .query("update wordcloud set Q1=@Q1, Q2=@Q2, Q3=@Q3, Q4=@Q4, Q5=@Q5,Q6=@Q6, Q7=@Q7, Q8=@Q8, Q9=@Q9,Q10=@Q10,Q11=@Q11, Q12=@Q12, Q13=@Q13, Q14=@Q14, Q15=@Q15, Q16=@Q16, Q17=@Q17, time=@time where email=@email and number=@number");
        console.log('update success');
        return updateUser.recordsets;
    } catch (error) {
        console.log('connect error', error);
    }
}

async function deleteUsers(userEmail) {
    try {
        let pool = await sql.connect(config);
        let deleteUser = await pool.request()
            .input('email', sql.NVarChar, userEmail)
            .query("delete from wordcloud where email=@email", (error, result) => {
                if (error) {
                    console.log('SQL error: ', error);// 資料庫存取有問題時回傳錯誤
                } else if (result.affectedRows === 1) {
                    console.log('delete succ!!!!!');
                } else {
                    console.log('delete fail!!!!!');
                }
            });
        return deleteUser.recordsets;
    } catch (error) {
        console.log('connect error', error);
    }
}

async function loginUsers(userEmail) {
    return new Promise(async (resolve, reject) => {
        try {
            var judge = false;
            let pool = await sql.connect(config)
            let loginUser = pool.request()
                .input('number', sql.Int, userEmail.number)
                .input('email', sql.NVarChar, userEmail.email)
                .query("select * from wordcloud where email=@email and number=@number", (err, result) => {
                    if (err) {
                        console.log(err);
                    }
                    var myarr = new Array();
                    if (result.rowsAffected == 1) {
                        judge = true;
                    }
                    for (var i = 0; i < result.recordset.length; ++i) {
                        var id = result.recordset[i].id;
                        var number = result.recordset[i].number;
                        var email = result.recordset[i].email;
                        var Q1 = result.recordset[i].Q1;
                        var Q2 = result.recordset[i].Q2;
                        var Q3 = result.recordset[i].Q3;
                        var Q4 = result.recordset[i].Q4;
                        var Q5 = result.recordset[i].Q5;
                        var Q6 = result.recordset[i].Q6;
                        var Q7 = result.recordset[i].Q7;
                        var Q8 = result.recordset[i].Q8;
                        var Q9 = result.recordset[i].Q9;
                        var Q10 = result.recordset[i].Q10;
                        var Q11 = result.recordset[i].Q11;
                        var Q12 = result.recordset[i].Q12;
                        var Q13 = result.recordset[i].Q13;
                        var Q14 = result.recordset[i].Q14;
                        var Q15 = result.recordset[i].Q15;
                        var Q16 = result.recordset[i].Q16;
                        var Q17 = result.recordset[i].Q17;
                        myarr.push({ 'id': id, 'number': number, 'email': email, 'Q1': Q1, 'Q2': Q2, 'Q3': Q3, 'Q4': Q4, 'Q5': Q5, 'Q6': Q6, 'Q7': Q7, 'Q8': Q8, 'Q9': Q9, 'Q10': Q10, 'Q11': Q11, 'Q12': Q12, 'Q13': Q13, 'Q14': Q14, 'Q15': Q15, 'Q16': Q16, 'Q17': Q17, 'judge': judge });

                    }
                    console.log(myarr)
                    resolve(myarr)
                    return myarr;
                });
        } catch (error) {
            reject(error)
            console.log('connect error', error)
        }
    })
}

module.exports = {
    getUser: getUser,
    getUserId: getUserId,
    addUsers: addUsers,
    updateUsers: updateUsers,
    deleteUsers: deleteUsers,
    loginUsers: loginUsers,
    addHealthUsers: addHealthUsers,
    checkHealthUsers: checkHealthUsers,
    createUsersType: createUsersType,
    checkUsersType: checkUsersType,
    updateUsersPoint1: updateUsersPoint1,
    updateUsersPoint2: updateUsersPoint2,
    updateUsersPoint3: updateUsersPoint3,
    updateUsersPoint4: updateUsersPoint4,
    updateUsersPoint5: updateUsersPoint5,
    updateUsersPoint6: updateUsersPoint6
}