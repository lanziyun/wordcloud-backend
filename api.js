require('./users_wordcloud');
const dboperations = require('./dboperation');

var express = require('express');
var bodyParser = require('body-parser'); //避免body undefined
var cors = require('cors');//避免跨域問題
const { request, response } = require('express');
const e = require('express');
var app = express();
var router = express.Router();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use('/api', router);

router.use((request, response, next) => {
    console.log('middleware');
    next();
})


router.route("/users").get((request, response) => {
    dboperations.getUser().then(result => {
        response.json(result[0]);
    });
})


// router.route("/users/:userid").get((request, response) => {
//     let id = request.params.userid;
//     dboperations.getUserId(id).then(result => {
//         response.json(result[0]);
//     });
// })

router.route("/users/create").post((request, response) => {
    let user = { ...request.body }
    dboperations.createUsersType(user).then(result => {
        response.send('新增單位成功')
    })
})

router.route("/users/updatePoint1").post((request, response) => {
    let user = { ...request.body }
    dboperations.updateUsersPoint1(user).then(result => {
        response.send('更新關卡1成功');
    });
});
router.route("/users/updatePoint2").post((request, response) => {
    let user = { ...request.body }
    dboperations.updateUsersPoint2(user).then(result => {
        response.send('更新關卡2成功');
    });
});
router.route("/users/updatePoint3").post((request, response) => {
    let user = { ...request.body }
    dboperations.updateUsersPoint3(user).then(result => {
        response.send('更新關卡3成功');
    });
});
router.route("/users/updatePoint4").post((request, response) => {
    let user = { ...request.body }
    dboperations.updateUsersPoint4(user).then(result => {
        response.send('更新關卡4成功');
    });
});
router.route("/users/updatePoint5").post((request, response) => {
    let user = { ...request.body }
    dboperations.updateUsersPoint5(user).then(result => {
        response.send('更新關卡5成功');
    });
});
router.route("/users/updatePoint6").post((request, response) => {
    let user = { ...request.body }
    dboperations.updateUsersPoint6(user).then(result => {
        response.send('更新關卡6成功');
    });
});

router.route("/users/checkType").post((request, response) => {
    let user = { ...request.body }
    dboperations.checkUsersType(user).then(result => {
        // console.log('Point', result)
        response.send(result)
    }).catch(err => {
        console.log(err)
    })
})


router.route("/users").post((request, response) => {
    let user = { ...request.body }
    dboperations.addUsers(user).then(result => {
        response.send('新增成功');
    });
})

router.route("/users/health").post((request, response) => {
    let user = { ...request.body }
    dboperations.addHealthUsers(user).then(result => {
        response.send('新增健康成功');
    });
})

router.route("/users/checkhealth").post((request, response) => {
    let check = { ...request.body }
    dboperations.checkHealthUsers(check).then(result => {
        // console.log('Health', result)
        response.send(result)
    })
        .catch(err => {
            console.log(err)
        })
})


router.route("/users/update").post((request, response) => {
    let user = { ...request.body }
    dboperations.updateUsers(user).then(result => {
        response.send('更新成功');
    });
});


// router.route("/users/delete/:email").get((request, response) => {
//     const email = request.params.email;
//     dboperations.deleteUsers(email).then(result => {
//         response.send('刪除成功');
//     }).catch((err) => { return response.send(err); });
// });

router.route("/users/login").post((request, response) => {
    let email = { ...request.body }
    // console.log(email)
    dboperations.loginUsers(email)
        .then(result => {
            console.log('Login', result)
            response.send(result)
        })
        .catch(err => {
            console.log(err)
        })
    // dboperations.loginUsers(email).then((result) => {
    //     // response.send({message: 'success'});
    //     console.log(JSON.stringify(result));
    //     response.send(JSON.stringify(result))
    // }).catch((err) => { return response.send(err); }); // 失敗回傳錯誤訊息
});

//var port = process.env.PORT || 8090;
//app.listen(port);
//console.log('user API is running at ' + port);
const hostname = "192.168.64.23";
var port = 8090;
const server = app.listen(port, hostname, () => {
    console.log(hostname, 'API is running at ' + port);
});

