class health {
    constructor(id, number, email, Q1, Q2, Q3, Q4, Q5, name, phone, contact_name, contact_phone, time) {
        this.id = id;
        this.number = number;
        this.email = email;
        this.Q1 = Q1;
        this.Q2 = Q2;
        this.Q3 = Q3;
        this.Q4 = Q4;
        this.Q5 = Q5;
        this.name = name;
        this.phone = phone;
        this.contact_name = contact_name;
        this.contact_phone = contact_phone;
        this.time = time;
    }
}